## Criando um Pipeline de Deploy com GitLab e Kubernetes
### Digital Innovation One - Especialista: Denilson Bonatti

# Descrição
Neste projeto cria um pipeline de deploy de uma aplicação com cenários de produção de imagens com Docker e criação dos deployments em um cluster kubernetes em nuvem utilizando o GCP.